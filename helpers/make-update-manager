#!/bin/sh
#
#    Copyright (C) 2008-2020  Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C)      2019  Mason Hock <mason@masonhock.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=10

. ./config

rm UpdateManager/Core/LivePatchSocket.py
rm tests/test_livepatch_socket.py
patch --no-backup-if-mismatch -p1 < $DATA/remove-livepatch.patch
sed -i '/import LivePatchSocket/d' UpdateManager/Dialogs.py
sed -i '/self.check_livepatch_status/d' UpdateManager/Dialogs.py

#Closes #12545
patch -p0 < $DATA/trisquel-versions.patch

cp $DATA/index.docbook help/C/index.docbook

# Prevent automated connections
patch -p1 < $DATA/prevent-autoconnection.patch
cat << EOF >> po/es.po

msgid ""
"To prevent unwanted connections to the Internet\n"
"Trisquel will not automatically check for updates\n"
"unless you say so in the Settings panel."
msgstr ""
"Para prevenir conexiones inesperadas a Internet\n"
"Trisquel no descargará actualizaciones automáticamente\n"
"salvo que lo indiques en el panel de Configuración."
EOF

replace internet Internet .
replace Ubuntu Trisquel .
replace TrisquelDistroInfo UbuntuDistroInfo .
replace "Trisquel-Gettext" "Ubuntu-Gettext" .

sed -i 's/Trisquel 16.04 LTS to Trisquel 18.04 LTS/Trisquel 8.0 LTS to Trisquel 9.0 LTS/' HweSupportStatus/consts.py

replace ubuntu-desktop trisquel .
replace kubuntu-desktop triskel .
replace xubuntu-desktop trisquel-mini .
replace edubuntu-desktop toast .

sed '/%s base/ s/name.*/name = "Trisquel base"/' -i ./UpdateManager/Core/UpdateList.py
sed 's_https://changelogs.ubuntu.com/changelogs/pool/_https://packages.trisquel.info/changelogs/pool/_' -i UpdateManager/Core/MyCache.py
sed '/len(changelog) == 0/,/"later."/d' -i UpdateManager/Core/MyCache.py

changelog "Compiled for Trisquel"

compile

